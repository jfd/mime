import {Assert} from "//silo.re/silo/1.0.0/Target.js";
import * as Mime from "../src/Mime.js";

export {lookup};


function lookup() {
    Assert.ok(Mime.lookup("test.txt") === "text/plain");
    Assert.ok(Mime.lookup("test.html") === "text/html");
    Assert.ok(Mime.lookup("test.adadsads") === "application/octet-stream");
}
